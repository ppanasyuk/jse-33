package ru.t1.panasyuk.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class TaskFindOneByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public TaskFindOneByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}