package ru.t1.panasyuk.tm.dto.request.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectFindOneByIdRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    public ProjectFindOneByIdRequest(@Nullable final String id) {
        this.id = id;
    }

}