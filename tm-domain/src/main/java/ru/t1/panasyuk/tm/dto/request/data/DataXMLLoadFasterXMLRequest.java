package ru.t1.panasyuk.tm.dto.request.data;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.t1.panasyuk.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public final class DataXMLLoadFasterXMLRequest extends AbstractUserRequest {
}