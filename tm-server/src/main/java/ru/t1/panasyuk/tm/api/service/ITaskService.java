package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Task;

public interface ITaskService extends IUserOwnedService<Task>, ITaskRepository {

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name);

    @NotNull
    Task updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    @NotNull
    Task updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}