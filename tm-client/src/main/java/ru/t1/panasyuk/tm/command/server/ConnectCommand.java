package ru.t1.panasyuk.tm.command.server;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.endpoint.IEndpointClient;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.api.service.IServiceLocator;
import ru.t1.panasyuk.tm.command.AbstractCommand;
import ru.t1.panasyuk.tm.enumerated.Role;

import java.net.Socket;

public final class ConnectCommand extends AbstractCommand {

    @NotNull
    public static String NAME = "connect";

    @NotNull
    public static String DESCRIPTION = "Connect to server.";

    @Override
    public void execute() {
        try {
            @NotNull final IServiceLocator serviceLocator = getServiceLocator();
            @NotNull final IPropertyService propertyService = serviceLocator.getPropertyService();
            @NotNull final IEndpointClient endpointClient = serviceLocator.getConnectionEndpointClient();
            @NotNull final String host = propertyService.getServerHost();
            @NotNull final Integer port = propertyService.getServerPort();
            endpointClient.setHost(host);
            endpointClient.setPort(port);
            @Nullable final Socket socket = endpointClient.connect();

            serviceLocator.getAuthEndpointClient().setSocket(socket);
            serviceLocator.getSystemEndpointClient().setSocket(socket);
            serviceLocator.getDomainEndpointClient().setSocket(socket);
            serviceLocator.getProjectEndpointClient().setSocket(socket);
            serviceLocator.getTaskEndpointClient().setSocket(socket);
            serviceLocator.getUserEndpointClient().setSocket(socket);
        } catch (@NotNull final Exception e) {
            getServiceLocator().getLoggerService().error(e);
        }
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}