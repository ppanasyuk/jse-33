package ru.t1.panasyuk.tm.client;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.endpoint.IEndpointClient;
import ru.t1.panasyuk.tm.dto.response.ApplicationErrorResponse;

import java.io.*;
import java.net.Socket;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractClient implements IEndpointClient {

    @NotNull
    protected String host = "localhost";

    @NotNull
    protected Integer port = 6060;

    @Nullable
    private Socket socket;

    public AbstractClient(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    public AbstractClient(@NotNull final AbstractClient client) {
        host = client.host;
        port = client.port;
        socket = client.socket;
    }

    @NotNull
    @SneakyThrows
    protected <T> T call(@NotNull final Object data, @NotNull final Class<T> clazz) {
        getObjectOutputStream().writeObject(data);
        @NotNull final Object result = getObjectInputStream().readObject();
        if (result instanceof ApplicationErrorResponse) {
            @NotNull final ApplicationErrorResponse response = (ApplicationErrorResponse) result;
            throw new RuntimeException(response.getMessage());
        }
        return (T) result;
    }

    @NotNull
    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    @NotNull
    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    @NotNull
    private OutputStream getOutputStream() throws IOException {
        if (socket == null) return null;
        return socket.getOutputStream();
    }

    @NotNull
    private InputStream getInputStream() throws IOException {
        if (socket == null) return null;
        return socket.getInputStream();
    }

    @Override
    @SneakyThrows
    public Socket connect() {
        socket = new Socket(host, port);
        return socket;
    }

    @Override
    @SneakyThrows
    public void disconnect() {
        if (socket == null) return;
        socket.close();
    }

}