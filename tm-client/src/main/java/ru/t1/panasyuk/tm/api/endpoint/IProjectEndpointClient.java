package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import java.net.Socket;

public interface IProjectEndpointClient extends IProjectEndpoint {

    void setSocket(@NotNull Socket socket);

}