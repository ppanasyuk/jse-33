package ru.t1.panasyuk.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.dto.request.data.DataJsonLoadFasterXMLRequest;

public final class DataJsonLoadFasterXMLCommand extends AbstractDataCommand {

    @NotNull
    private static final String DESCRIPTION = "Load data from json file.";

    @NotNull
    private static final String NAME = "data-load-json-fasterxml";

    @SneakyThrows
    @Override
    public void execute() {
        System.out.println("[DATA LOAD JSON]");
        @NotNull final DataJsonLoadFasterXMLRequest request = new DataJsonLoadFasterXMLRequest();
        getDomainEndpoint().loadDataJsonFasterXML(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
