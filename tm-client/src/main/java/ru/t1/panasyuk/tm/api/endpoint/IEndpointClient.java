package ru.t1.panasyuk.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;

import java.net.Socket;

public interface IEndpointClient {

    Socket connect();

    void disconnect();

    void setHost(@NotNull String host);

    void setPort(@NotNull Integer port);

}