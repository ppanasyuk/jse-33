package ru.t1.panasyuk.tm.client;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpointClient;
import ru.t1.panasyuk.tm.dto.request.user.*;
import ru.t1.panasyuk.tm.dto.response.user.*;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractClient implements IAuthEndpointClient {

    public AuthEndpointClient(@NotNull final AbstractClient client) {
        super(client);
    }

    @NotNull
    @Override
    public UserLoginResponse loginUser(@NotNull final UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logoutUser(@NotNull final UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserViewProfileResponse viewUserProfile(@NotNull final UserViewProfileRequest request) {
        return call(request, UserViewProfileResponse.class);
    }

}